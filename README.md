# README #


### What is this repository for? ###

The goal for the project is to replace the current paper based system for ordering commodities with a web-based app connected to DHIS2. This will enable users to submit orders to higher managements more efficiently than when using today's system. On one side the app is a simple web-based form where the health workers can fill in orders. On the other side the app is notification system, where the management can be notified of order requests. 

### How do I get set up? ###

To test the application just download the .zip file and upload it to play.dhis2.org.


### Made by ###

Simen Røste Odden
Thomas Schwitalla
Jørgen Valen