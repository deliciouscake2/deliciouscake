var me;

/*GETS a JSON representing the currently logged in user from the 
  me resource, before copying the object into the global variable *me*. 
*/
function setCurrentUser(){

    var meRequest = $.ajax({
	type: 'GET',
	url: 'https://play.dhis2.org/demo/api/me.json',
	contentType: 'application/json',
	crossDomain: true
    });
    
    meRequest.success(function(json) {
	console.log("Successfully got me.");
	console.log(json);
	me = JSON.parse(JSON.stringify(json)); //Hack for copying the object.
    }); 
    
    meRequest.error(function( jqXHR, textStatus, errorThrown) {	
	console.log("You don't get me.");
	console.log(textStatus+": " + errorThrown);
    });
}

/* POSTS the passed message string to messageConversations. On success,
   a message appears in place of the on-screen order summary on the app
   main page that confirms that the order was sent. Additionally, a 
   confirmation message is sent only to the inbox of the current user (me).
   A different message is displayed if something goes wrong when sending the
   order, in which case no confirmation will be sent.
   Parameter *order*: String of the order that will be sent. See 
   makeOrder() and sendMessage() for more details.
   Parameter *confirmation*: String of the confirmation message that will be
   sent in case the order was successfully placed.
*/
function postOrderMessage(order, confirmation){
    var request = $.ajax({
	type: 'POST',
	url: 'https://play.dhis2.org/demo/api/messageConversations',
	dataType: 'json',
	contentType: 'application/json',
	crossDomain: true,	
	data: JSON.stringify(order)	
    });

    request.success(function(data) {
	//$( this ).addClass( "success" );
	console.log("Message sent!");
	$('#feedbackHeader').empty();
	$('#feedbackButton').remove();
	$('#feedbackHeader').append("<h3>Your order is sent. Following is ordered:</h3>");
	getCommodityResource(); //reset commodity form.
	postConfirmationMessage(confirmation);
	
    });

    request.error(function( jqXHR, textStatus, errorThrown) {
	console.log(textStatus+": " +errorThrown);
	$('#feedback').append("<h3>Error when sending message. Please try again</h3>");
    });   
}

/* POSTs a message to the me's inbox confirming the successful sending of a
   commodity order.
   To be called by postOrderMessage only when its AJAX call succeded. 
*/
function postConfirmationMessage(confirmation){
    var request = $.ajax({
	type: 'POST',
	url: 'https://play.dhis2.org/demo/api/messageConversations',
	dataType: 'json',
	contentType: 'application/json',
	crossDomain: true,	
	data: JSON.stringify(confirmation)	
    });
     
    request.success(function(data) {
	console.log(data);
	$('#feedbackHeader').append("<h3><p>A confirmation of the order has been sent to your DHIS2 inbox.</h3>");
    });

    request.error(function( jqXHR, textStatus, errorThrown) {
	console.log(textStatus+": " +errorThrown);
    });
    
}

/*Builds messages for both orders and confirmations and calls the post 
  function in order to send them.*/
function sendMessage() {
    
    /*NOTE: Order Message recipients are currently hard-coded, as the 
      question as to whom should receive messages still remains unanswered.*/
    var users = [{id:"OYLGMiazHtW"}, {id:"N3PZBUlN8vq"}];
    var userGroups = [{id:"ZoHNWQajIoe"}];
    var organisationUnits = [{id:"DiszpKrYNg8"}];
    
    var list = commodityList("The following is a list of requested commodities");
    var order = makeMessage(list, "Commodity order form submitted",
			  users, userGroups, organisationUnits);

    var confirmation = makeMessage(list, "Confirmation of commodity order.",
    				 [{id: me.id}], [], []);

    postOrderMessage(order, confirmation);

}

/* Builds and returns a string out of the list of commodities that 
   have been entered in the order form.
   Parameter *header*: A string of words that will be shown as the first 
   line in the message body.
*/
function commodityList (header){
    var list = header + "\n";
    var input = document.getElementsByName("myInputs[]");
    var length = input.length;

    //iterating over all inputs and adding the values to list
    for (var i = 0; i < length; i++) {
        if (input[i].value !== "0") {
	    list += commodityNames[i] + ": " + input[i].value + "\n ";
	}
    }
    return list;
}

/* Constructs and returns a JSON object representing a message containing
   that is to be sent to messageConversations. For use when ordering 
   commodities and when sending confirmations of such orders. 
   Parameter *body*: String containing the message body.
   Parameter *subject*: String containing the message subject.
   Parameter *users*: Array of objects of individual recipients of the
   message.
   Parameter *userGroups*: Array of objects of group recipients.
   Parameter *orgUnits*: Array of objects of organisation units.
   NOTE: All parameters are required, but may be passed as empty arrays 
   or objects. There needs to be at least one object that represents
   a valid recipient.
*/
function makeMessage(body, subject, users, userGroups, organisationUnits) {
    
    var json = new Object();
   
    json.subject = subject;
    json.text = body;
    json.users = users;
    json.userGroups = userGroups;
    json.organisationUnits = organisationUnits;
    console.log("makeMessage: Built message JSON: " + json);

    return json;
}
