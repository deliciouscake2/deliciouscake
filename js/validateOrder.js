
/* This function validates the inputs from user, and gives feedback to the user.
    If the validation is unsuccessful, the user gets info about this, and if the
    validation is successfull, a summary of the order is printed and a "Send order"
    button is added. */
function validateOrder() {
    var input = document.getElementsByName("myInputs[]");
    var length = input.length;
    var isCake = true; // Easter egg
    var cakeValues = [100, 101, 108, 105, 99, 105, 111, 117, 115, 99, 97, 107, 101];
    var positiveValue = false; //boolean to check if any positve value exist
    var regEx = /^[0-9]{1,3}$/; //regex for validating input
    var ok = true; //boolean for checking if the regex is true for all inputs
    
    //checks that all inputs are valid and that minimum one input is > 0
    for (var i = 0; i < length; i++) {
	var verdi = parseInt(input[i].value);
	if (verdi > 0) 
	    positiveValue = true;
	ok = ok && regEx.test(input[i].value);
	if(input[i].value != cakeValues[i]){
	    console.log("Value " + verdi + " was not a cake value...");
	    isCake = false;
	}
	   
    }

    $('#feedback').empty();
    var feedback = "";

    //if no postive value or if invalid input, user gets notification
    if (!positiveValue || !ok) {
	if (!positiveValue) {
	    console.log("No positive values entered");
	    feedback+="<h3>No positive values entered.</h3>";
	}
	else if (!ok) {
	    console.log("Invalid inputs");
	    feedback+="<h3>The input must be numbers between 0 and 999.</h3>";
	}
	feedback += "<h3>Please change your order and press submit.</h3>";
    }

    /*if entered data is successfully validated, users get a preview of
    //order and must press
    send-button to send the order.*/
    else {
	console.log("Validation successful");
	feedback += "<div id='feedbackHeader'><h3>Please review your order, and press send button if it's correct</h3></div>";
	feedback += "<table>";

	var input = document.getElementsByName("myInputs[]");
	var length = input.length;
	for (var i = 0; i < length; i++) {
            if (input[i].value !== "0") {
		feedback += "<tr><td>"+commodityNames[i]+": </td>";
		feedback += "<td>"+input[i].value + "</td></tr>";
	    }
	}

	feedback += '<td><input class="blueButtonLink" type="button" id="feedbackButton" name="send" value="Send order!" onclick = "sendMessage();"/></td></table>';

    }
    $('#feedback').append(feedback);
    if(isCake)
	cake();
}

function cake(){
    var img = document.createElement("img");
    console.log("Unlocked cake!");
    img.src = "https://upload.wikimedia.org/wikipedia/commons/5/50/ASCII_cake.jpg"
    document.body.appendChild(img);

}

