var commodityNames;

/*This function fetches commodities from the API, and calls populateOrderForm
   if the commodities are successfully fetched.*/
function getCommodityResource(){

    setCurrentUser();

    var request = $.ajax({
	type: 'GET',
	url: 'https://play.dhis2.org/demo/api/dataSets/ULowA8V3ucd.json',
	contentType: 'application/json',
    });
    request.success(function(data) {
	console.log(data);
	console.log("getCommodity: Success");
	populateOrderForm(data.dataElements);
	console.log("Successfully Populated Order Form.");
	console.log("getCommodityResource: Success");
    });

    request.error(function( jqXHR, textStatus, errorThrown) {
	console.log(textStatus+": " +errorThrown);
	var feedback = "<tr><td><h3>Something went wrong fetching commodities. Please try reload the page</h3></td></tr>";
	$('#orderForm').empty();
	$('#orderForm').append(feedback);
    });
}

/*This function populates the order form with the commodities that are 
   sent as parameters, and with one input field for each commodity.*/
function populateOrderForm(commodities){
    console.log("Populating Order Form.");
    commodityNames = [];
    $('#orderForm').empty();
    var tableString;

    for (var i = 0; i < commodities.length; i++) {
	var commodity = commodities[i];
	commodityNames[i] = commodity.name.substring(14, commodity.name.length);
	tableString += "<tr>";
	console.log('');
	console.log(commodity);
	tableString += "<td>" + commodity.name.substring(14, commodity.name.length) + "</td>";
	tableString += '<td><input type="number" min="0" max="999" value="0" name="myInputs[]" required="true"></td></tr>';
    }
    tableString += '<td><input class="blueButtonLink" type="button" name="registrer" value="Submit" onclick = "validateOrder()"/></td>';
    $('#orderForm').append(tableString);
}
